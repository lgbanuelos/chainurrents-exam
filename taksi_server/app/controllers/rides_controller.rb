class RidesController < ApplicationController
  def query_history
    customer_id = params[:customerId]
    start_date = DateTime.parse(params[:startDate])
    end_date = DateTime.parse(params[:endDate])

    render text: {prompt: "Balance for period #{start_date} to #{end_date}"}.to_json
  end
end
