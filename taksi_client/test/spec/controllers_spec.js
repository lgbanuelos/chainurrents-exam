'use strict';

describe('Test for RidesHistoryController', function () {

  beforeEach(module('taksi'));

  var RidesHistoryController,
    scope,
    $httpBackend;

  beforeEach(inject(function ($controller, $rootScope, _$httpBackend_) {
    scope = $rootScope.$new();
    $httpBackend = _$httpBackend_;

    RidesHistoryController = $controller('RidesHistoryController', {
      $scope: scope
    });
  }));

  it('should always fail', function () {
    expect(true).toBe(false);
  });
});
