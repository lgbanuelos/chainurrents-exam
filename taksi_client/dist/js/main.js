var app = angular.module('taksi', ['ngRoute']);

app.config(function($routeProvider){
    $routeProvider
        .when('/rides/history', {
            templateUrl: 'views/rides/history.html',
            controller: 'RidesHistoryController'
        })
        .otherwise({
            redirectTo: '/rides/history'
        });
});
