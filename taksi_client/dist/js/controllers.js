'use strict';

var app = angular.module('taksi');

app.controller('RidesHistoryController', function ($scope, $http) {
  $scope.history = [];

  $scope.submitQuery = function() {
    $http.post('http://localhost:3000/rides/history',
      {startDate: $scope.startDate, endDate: $scope.endDate, customerId: $scope.customerId})
      .then(function (response) {
        $scope.history = response.data;
      });
  };
});
